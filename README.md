
This plug-in for CKEditor provides integration to insert source code blocks into the WYSIWYG editor with code-friendly editor capabilities vi CodeMirror.

CKEditor (http://ckeditor.com/) is distributed under the GPL, LGPL, and MPL Open Source licenses (http://ckeditor.com/license)
CodeMirror (http://codemirror.net/) is released under a MIT-style license (http://codemirror.net/LICENSE)
Icon used for toolbar button is part of the Fugue Icons set by Yusuke Kamiyamane (http://p.yusukekamiyamane.com/) available under Creative Commons Attribution 3.0 License (http://creativecommons.org/licenses/by/3.0/)

This plug-in is subject to the terms of the Mozilla Public License, v. 2.0. (http://mozilla.org/MPL/2.0/).

Copyright (C) 2012, Kevin Walton. All rights reserved.
