﻿/*********************************************************************************************************/
/**
 * syntaxhighlight plugin for CKEditor 3.x for SyntaxHighlighter 3.0.83
 * Released: On 2011-06-24
 * Download: http://www.harrisonhills.org/techresources
 * Original plugin written by Lajox found at http://code.google.com/p/lajox
 */
/*********************************************************************************************************/

CKEDITOR.plugins.add('syntaxhighlight',   
  {    
    requires: ['dialog'],
	lang : ['en'], 
    init:function(a) { 
	var b="syntaxhighlight";
	var c=a.addCommand(b,new CKEDITOR.dialogCommand(b));
		c.modes={wysiwyg:1,source:0};
		c.canUndo=false;
	a.ui.addButton("syntaxhighlight",{
					label:a.lang.syntaxhighlight.title,
					command:b,
					icon:this.path+"images/syntaxhighlight.gif"
	});
	CKEDITOR.dialog.add(b,this.path+"dialogs/syntaxhighlight.js")},
      afterInit : function( editor )
      {
          var path = this.path; //capture in closure for contentDom eventHandler

          //HACK: get handle to iframe content document and append our styles and js to it
          editor.on( 'contentDom', function( e ){
              var doc = e.editor.document.$;

              function scriptElem( file ){
                  var elem = document.createElement('script');
                  elem.setAttribute("type","text/javascript");
                  elem.setAttribute("src", file);
                  return elem;
              }
              function styleElem( file ){
                  var elem = document.createElement("link");
                  elem.setAttribute("type", "text/css");
                  elem.setAttribute("rel", "stylesheet");
                  elem.setAttribute("href", file);
                  return elem;
              }

              function loadScripts( arr ){
                  var f = arr.shift();
                  var s = scriptElem(path+"syntaxhighlighter/scripts/"+f);
                  doc.head.appendChild( s );

                  if( arr.length > 0 ){
                      s.onload = function(){
                          loadScripts(arr);
                      }
                  }
              }

              doc.head.appendChild( styleElem(path+"hide_code.css") );
              doc.head.appendChild( styleElem(path+"syntaxhighlighter/styles/shCoreDefault.css") );
              doc.head.appendChild( styleElem(path+"syntaxhighlighter/styles/shThemeDefault.css") );

              //doc.head.appendChild( scriptElem(path+"syntaxhighlighter/scripts/xregexp-min.js") );
              //doc.head.appendChild( scriptElem(path+"syntaxhighlighter/scripts/shCore.js") );
              //doc.head.appendChild( scriptElem(path+"syntaxhighlighter/scripts/shBrushJScript.js") );
              loadScripts( ["xregexp-min.js","shCore.js","shBrushJScript.js","shBrushXml.js"] );

              //<script type="text/javascript">SyntaxHighlighter.all();</script>
          });
      }
});