/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

/**
 * @fileOverview The "codeblock" plugin.
 *
 */

(function()
{
	//var codeblockReplaceRegex = /\[\<\[[^\]]+\]\>\]/g;
    //var codeblockReplaceRegex = /\<pre\>\<code\>.+?\<\/code\>\<\/pre\>/g;
    var codeblockReplaceRegex = /\<div data\-cke.+?\<\/div\>/g;
	CKEDITOR.plugins.add( 'codeblock',
	{
        requires : [ 'dialog' ],
		//requires : [ 'iframedialog' ],
		init : function( editor )
		{
			//var lang = editor.lang.codeblock;

			editor.addCommand( 'createcodeblock', new CKEDITOR.dialogCommand( 'createcodeblock' ) );
			editor.addCommand( 'editcodeblock', new CKEDITOR.dialogCommand( 'editcodeblock' ) );
            /*
            editor.addCommand( 'createcodeblock', new CKEDITOR.command( editor,
                {
                    exec : function( editor )
                    {
                        document.getElementById("codeeditor").style['display'] = "block";
                    }
                }
            ) );
            editor.addCommand( 'editcodeblock',
                document.getElementById("codeeditor").style['display'] = "block"
            );
            */

			editor.ui.addButton( 'CreateCodeBlock',
			{
				label : "lang.toolbar",
				command :'createcodeblock',
				icon : this.path + '1342924348_edit-code.png'
			});

			if ( editor.addMenuItems )
			{
				editor.addMenuGroup( 'codeblock', 20 );
				editor.addMenuItems(
					{
						editcodeblock :
						{
							label : "Edit Source Code",
							command : 'editcodeblock',
							group : 'codeblock',
							order : 1,
							icon : this.path + 'codeblock.gif'
						}
					} );

				if ( editor.contextMenu )
				{
                    function ancestorData( elem, dataName ){
                        //traverse up looking for data
                        var d = elem.data(dataName);
                        if( d ){
                            return d;
                        }else if( elem.getParent() ){
                            return ancestorData( elem.getParent(), dataName );
                        }
                        //if no more ancestors
                        return false;
                    }

					editor.contextMenu.addListener( function( element, selection )
						{
							if ( !element || (!element.data( 'cke-codeblock' ) && !ancestorData(element, 'cke-codeblock')) )
								return null;

							return { editcodeblock : CKEDITOR.TRISTATE_OFF };
						} );
				}
			}

			editor.on( 'doubleclick', function( evt )
				{
					if ( CKEDITOR.plugins.codeblock.getSelectedCodeblock( editor ) )
						evt.data.dialog = 'editcodeblock';
				});

			editor.addCss(
				'pre.cke_codeblock' +
				'{' +
					'background-color: #fff8f1; border: dashed 1px #EEEEEE;' +
					( CKEDITOR.env.gecko ? 'cursor: default;' : '' ) +
				'}'
			);

			editor.on( 'contentDom', function()
				{
					editor.document.getBody().on( 'resizestart', function( evt )
						{
							if ( editor.getSelection().getSelectedElement().data( 'cke-codeblock' ) )
								evt.data.preventDefault();
						});
				});

			CKEDITOR.dialog.add( 'createcodeblock', this.path + 'dialogs/codeblock.js' );
			CKEDITOR.dialog.add( 'editcodeblock', this.path + 'dialogs/codeblock.js' );
		},
		afterInit : function( editor )
		{
            var path = this.path+"../syntaxhighlight/"; //capture in closure for contentDom eventHandler

            //HACK: get handle to iframe content document and append our styles and js to it
            editor.on( 'contentDom', function( e ){
                var doc = e.editor.document.$;

                function scriptElem( file ){
                    var elem = document.createElement('script');
                    elem.setAttribute("type","text/javascript");
                    elem.setAttribute("src", file);
                    return elem;
                }
                function styleElem( file ){
                    var elem = document.createElement("link");
                    elem.setAttribute("type", "text/css");
                    elem.setAttribute("rel", "stylesheet");
                    elem.setAttribute("href", file);
                    return elem;
                }

                function loadScripts( arr ){
                    var f = arr.shift();
                    var s = scriptElem(path+"syntaxhighlighter/scripts/"+f);
                    doc.head.appendChild( s );

                    if( arr.length > 0 ){
                        s.onload = function(){
                            loadScripts(arr);
                        }
                    }
                }

                doc.head.appendChild( styleElem(path+"hide_code.css") );
                doc.head.appendChild( styleElem(path+"syntaxhighlighter/styles/shCoreDefault.css") );
                doc.head.appendChild( styleElem(path+"syntaxhighlighter/styles/shThemeDefault.css") );

                //doc.head.appendChild( scriptElem(path+"syntaxhighlighter/scripts/xregexp-min.js") );
                //doc.head.appendChild( scriptElem(path+"syntaxhighlighter/scripts/shCore.js") );
                //doc.head.appendChild( scriptElem(path+"syntaxhighlighter/scripts/shBrushJScript.js") );
                loadScripts( ["xregexp-min.js","shCore.js","shBrushJScript.js","shBrushXml.js"] );

                //<script type="text/javascript">SyntaxHighlighter.all();</script>
            });
            /////////////////////////////////////////////////////////////////////////////////////////////////


			var dataProcessor = editor.dataProcessor,
				dataFilter = dataProcessor && dataProcessor.dataFilter,
				htmlFilter = dataProcessor && dataProcessor.htmlFilter;

			if ( dataFilter )
			{
				dataFilter.addRules(
				{
					text : function( text )
					{
						return text.replace( codeblockReplaceRegex, function( match )
							{
								return CKEDITOR.plugins.placeholder.createCodeblock( editor, null, match, 1 );
							});
					}
				});
			}

			if ( htmlFilter )
			{
				htmlFilter.addRules(
				{
					elements :
					{
						'span' : function( element )
						{
							if ( element.attributes && element.attributes[ 'data-cke-codeblock' ] )
								delete element.name;
						}
					}
				});
			}
		}
	});
})();

CKEDITOR.plugins.codeblock =
{
	createCodeblock : function( editor, oldElement, text, isGet )
	{
        if( oldElement ){
            oldElement = oldElement.getParent();
        }
    
		var prelement = new CKEDITOR.dom.element( 'pre', editor.document );
        var element = new CKEDITOR.dom.element( 'code', editor.document );
        prelement.append(element);
        prelement.setAttributes({'class':'cke_codeblock',contentEditable:'false'});
		element.setAttributes(
			{
				contentEditable		: 'false',
				'data-cke-codeblock'	: 1,
				'class'			: 'cke_codeblock brush: js'
			}
		);

		text && element.setText( text );

		if ( isGet )
			return prelement.getOuterHtml();

		if ( oldElement )
		{
			if ( CKEDITOR.env.ie )
			{
				prelement.insertAfter( oldElement );
				// Some time is required for IE before the element is removed.
				setTimeout( function()
					{
						oldElement.remove();
						element.focus();
					}, 10 );
			}
			else
				prelement.replace( oldElement );
		}
		else
			editor.insertElement( prelement );



        var h=editor;//this.getParentEditor();

        h.window.$.SyntaxHighlighter.config.stripBrs = false;
        h.window.$.SyntaxHighlighter.config.tagName = "code";
        h.window.$.SyntaxHighlighter.highlight( {}, element.$ );    //trigger dom parse to highlight newly created/updated pre area... note ".all()" only works on page load


        var ht = editor.document.$.body.innerHTML;
        editor.document.$.body.innerHTML = ht = ht.replace('div id="highlighter_', 'div data-cke-codeblock="1" id="highlighter_');


        return null;
	},

	getSelectedCodeblock : function( editor )
	{
		var range = editor.getSelection().getRanges()[ 0 ];
		range.shrink( CKEDITOR.SHRINK_TEXT );
		var node = range.startContainer;
		while( node && !( node.type == CKEDITOR.NODE_ELEMENT && node.data( 'cke-codeblock' ) ) )
			node = node.getParent();
		return node;
	}
};
