/*
 * Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

(function()
{
    var cmEditor = null;
	function codeblockDialog( editor, isEdit )
	{

		//var lang = editor.lang.codeblock,
		var generalLabel = "codeblock";//editor.lang.common.generalTab;
		return {
			title : "Code Editor",
			minWidth : editor.container.$.clientWidth - 50,
			minHeight : editor.container.$.clientHeight - 160,
			contents :
                [
                    {
                        id : 'codeeditor',
                        expand : true,
                        elements :
                            [
                                {
                                    type : 'textarea',
                                    id : 'contents',
                                    required : true,
                                    setup : function( element )
                                    {
                                        //if( !cmEditor ){
                                        cmEditor = CodeMirror.fromTextArea(document.getElementById(this.domId), {
                                            mode: "javascript",
                                            lineNumbers: true,
                                            matchBrackets: true,
                                            tabMode: "indent"
                                        });
                                        //}
                                        /*
                                        new CodeMirrorUI( document.getElementById( this.domId ),
                                            {
                                                path : 'js/',
                                                searchMode : 'popup'
                                            },
                                            {
                                                mode: "javascript"
                                            });
                                        */

                                        if ( isEdit ){
                                            //this.setValue( element.getText().slice( 2, -2 ) );
                                            cmEditor.setValue( element.getText() );
                                        }
                                    },
                                    commit : function( element )
                                    {
                                        //var text = '[<[' + this.getValue() + ']>]';
                                        var text = cmEditor.getValue();
                                        // The placeholder must be recreated.
                                        CKEDITOR.plugins.codeblock.createCodeblock( editor, element, text );
                                    }
                                }
                                /*
                                {
                                    type : 'iframe',
                                    src : editor.plugins.codeblock.path + 'dialogs/codeeditor.html',
                                    width : '100%',
                                    height : '100%',
                                    onContentLoad : function() {
                                        // Iframe is loaded...
                                        var iframe = document.getElementById( this._.frameId );
                                        iframeWindow = iframe.contentWindow;
                                    }
                                }
                                */
                            ]
                    }
                ]
			/*[

				{
					id : 'info',
					label : generalLabel,
					title : generalLabel,
					elements :
					[
						{
							id : 'text',
							type : 'text',
							style : 'width: 100%;',
							label : "lang.text",
							'default' : '',
							required : true,
							validate : CKEDITOR.dialog.validate.notEmpty( "lang.textMissing" ),
							setup : function( element )
							{
								if ( isEdit )
									//this.setValue( element.getText().slice( 2, -2 ) );
                                    this.setValue( element.getText() );
							},
							commit : function( element )
							{
								//var text = '[<[' + this.getValue() + ']>]';
                                var text = this.getValue();
								// The placeholder must be recreated.
								CKEDITOR.plugins.codeblock.createCodeblock( editor, element, text );
							}
						}
					]
				}
			]*/,
			onShow : function()
			{
				if ( isEdit )
					this._element = CKEDITOR.plugins.codeblock.getSelectedCodeblock( editor );

				this.setupContent( this._element );
                
                cmEditor.focus();
			},
			onOk : function()
			{
                //var val = iframeWindow.getElementById('code1').value;
                //this.commitContent( val );
				this.commitContent( this._element );
                cmEditor.toTextArea();
                cmEditor = null;
				delete this._element;
			},
            onCancel : function()
            {
                cmEditor.toTextArea();
                cmEditor = null;
                delete this._element;
            }
		};
	}

	CKEDITOR.dialog.add( 'createcodeblock', function( editor )
		{
			return codeblockDialog( editor );
		});
	CKEDITOR.dialog.add( 'editcodeblock', function( editor )
		{
			return codeblockDialog( editor, 1 );
		});
} )();
